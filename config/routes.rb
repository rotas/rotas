Rails.application.routes.draw do

  resources :rotas

  get "admin" => "rotas#admin"

  get 'percursos/:id', to: 'percursos#show', :defaults => { :format => :json }, as: :percurso
  match "percursos" => "percursos#create", :via => :post, :as => :create_percurso
  match "percursos/:id" => "percursos#update", :defaults => { :format => :json }, :via => :patch, :as => :update_percurso

  root "rotas#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
