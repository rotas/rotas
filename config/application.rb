require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RotasBackend
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.i18n.default_locale = :"pt-BR"
    I18n.enforce_available_locales = false
    config.time_zone = "Brasilia"

    config.google_maps_key = ENV["GOOGLE_MAPS_KEY"]
    config.admin_password = ENV["ROTAS_ADMIN_PASSWORD"]
    config.community_token = ENV["ROTAS_COMMUNITY_TOKEN"]

  end
end
