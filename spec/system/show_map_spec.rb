require 'rails_helper'

Rails.application.load_seed

RSpec.describe 'Home page', type: :system do
  it 'mostra rotas conhecidas' do
    visit '/'
    expect(page).to have_text('Cavalo => P1')
    expect(page).to have_text('Serpro => Quitaúna')
  end

  context 'usando rota com percurso definido' do
    it 'abre página e mostra mapa' do
      visit '/'
      nome_rota = find('td', text: 'Cavalo => P1')
      tr = nome_rota.find(:xpath, '..')
      tr.click_link 'Mostrar no mapa'
      expect(page).to have_text('Nome: Cavalo => P1')
      expect(page).to have_text('Partida: 14:00')
      expect(page).to have_text('Tipo veiculo: ônibus')
      expect(page).to have_text('Voltar')
      expect(page).to have_selector('div#map')
    end
  end
end
