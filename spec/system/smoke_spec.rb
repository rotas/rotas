require 'rails_helper'

RSpec.describe 'Home page', :type => :system do
  it 'displays the app name' do
    visit '/'
    expect(page).to have_text('Rotas')
  end
end
