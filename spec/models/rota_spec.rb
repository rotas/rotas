require 'rails_helper'

# TODO: improve error messages, such as in rota_test.rb

RSpec.describe Rota, type: :model do
  it 'cria nova rota' do
    rota = Rota.new(nome: 'USP', partida: '07:00', tipo_veiculo: 'ônibus')
    expect(rota.save).to be true
  end

  it 'não cria rota sem nome' do
    rota = Rota.new(partida: '07:00', tipo_veiculo: 'ônibus')
    expect(rota.save).to be false
  end

  it 'não cria rota com partida inválida' do
    rota = Rota.new(partida: '7 horas', tipo_veiculo: 'ônibus')
    expect(rota.save).to be false
  end

  it 'não cria rota com tipo veículo inválido' do
    rota = Rota.new(nome: 'USP', partida: '07:00', tipo_veiculo: 'bike')
    expect(rota.save).to be false
  end

  it 'cria rotas com todos os tipos de veículo possíveis' do
    # Obs: create! joga exceção se não conseguir criar objeto
    Rota.create!(nome: 'USP', partida: '07:00', tipo_veiculo: 'ônibus')
    Rota.create!(nome: 'USP', partida: '07:00', tipo_veiculo: 'van')
  end
end
