# Rotas 

Rotas é um sistema para o compartilhamento de rotas de uma comunidade.

![screenshot](public/screenshot.png)

Uma rota possui informações básicas (nome, horário, tipo de veículo) e um percurso. O percurso consiste em um ponto de partida, um ponto de chegada e pontos intermediários que podem ser, ou não, pontos de parada.

A comunidade pode contribuir com os percursos por meio da API de percursos. Isso possibilita métodos inteligentes de geração de percursos, como por exemplo 'aplicativo móvel que captura posições via GPS' ou 'planilha de endereços que é convertida para pontos geolocalizados'.

## Funcionalidades do sistema web (rotas-backend)

* Cadastro de rotas
* Exibição de percursos no mapa
* Recepção de percursos via API

## Setup do projeto

[Setup](/doc/SETUP.md)

## TODOs

* Testes
