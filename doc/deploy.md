# Deploy

Instale o Heroku Cli.

Aí `heroku login`.

Pra ver apps Heroku existentes da sua conta: `heroku apps`.

Criei o app ruby "fretados-socorro" pelo console web.

Criando o remote git: `heroku git:remote -a fretados-socorro`.

Deploy: `git push heroku master`.

Setando as variáveis de ambiente:

```
heroku config:set GOOGLE_MAPS_KEY=...
heroku config:set ROTAS_ADMIN_PASSWORD=...
heroku config:set ROTAS_COMMUNITY_TOKEN=...
```

Para conferir as variáveis: `heroku config`.

Rodando migrations: `heroku run rake db:migrate`

Obs: nem mexi no database.yml e funcionou a conexão com o banco!

Obs: para acessar o banco de produção: `heroku pg:psql`.

