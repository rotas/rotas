
# Setup do projeto 

Sistema operacional utilizado: Ubuntu 18.04.1 LTS.

## Instalação do Ruby e dependências 

Instalar o Ruby 2.6.0 seguindo os passos em 
https://github.com/PoliGNU/sitedeploy/blob/master/doc/ruby.md

Instalar também:

```
sudo apt-get install nodejs sqlite3 libsqlite3-dev 
```

No diretório do projeto: `bundle install`.

## Instalando e configurando o postgres

```
$ sudo apt-get install postgresql postgresql-contrib libpq-dev
$ echo 'localhost:5432:*:postgres:postgres' >> ~/.pgpass
$ chmod 600 ~/.pgpass
$ sudo su postgres
$ psql
# \password postgres
```

Aí vai aparecer o prompt para você definir a senha do postgres. Defina como senha 'postgres'.

Pra ver que deu certo, abra um novo terminal e tente acessar o postgres com o comando `psql -h localhost -U postgres`.

## Google cloud

Criar chave de API e associar às APIs "Maps JavaScript API" e "Directions API".
 
## Configuração inicial

```
echo 'export GOOGLE_MAPS_KEY=sua_chave_aqui' >> ~/.bashrc
echo 'export ROTAS_ADMIN_PASSWORD=admin' >> ~/.bashrc
echo 'export ROTAS_COMMUNITY_TOKEN=community' >> ~/.bashrc
source ~/.bashrc
```

No diretório do projeto: `rails db:setup`.

## Uso diário

Para iniciar o serviço localmente: `rails server`.

Sempre que quiser reiniciar o banco para o estado original: `rails db:setup`.

Quando houver novas migrations: `rails db:migrate`.

Para executar os testes: `rspec`.

Obs sobre tipos de testes no rspec:

* Usar model, controller e system tests.
* Não usar request e feature tests.
