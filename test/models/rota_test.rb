require 'test_helper'

class RotaTest < ActiveSupport::TestCase

  test "Criar rota" do
    rota = Rota.new(nome: "USP", partida: "07:00", tipo_veiculo: "ônibus") 
    assert rota.save, "Não criou a rota"
  end

  test "Não criar rota sem nome" do
    rota = Rota.new(partida: "07:00", tipo_veiculo: "ônibus") 
    assert_not rota.save, "Criou rota sem nome"
  end

  test "Não criar rota com partida inválida" do
    rota = Rota.new(partida: "7 horas", tipo_veiculo: "ônibus") 
    assert_not rota.save, "Criou rota com partida inválida"
  end

  test "Não criar rota com tipo veículo inválido" do
    rota = Rota.new(nome: "USP", partida: "07:00", tipo_veiculo: "bike") 
    assert_not rota.save, "Criou rota com tipo veículo inválido"
  end

  test "Criar rotas com todos os tipos de veículo possíveis" do
    assert Rota.create!(nome: "USP", partida: "07:00", tipo_veiculo: "ônibus"), 
      "Não criou a rota com ônibus"
    assert Rota.create!(nome: "USP", partida: "07:00", tipo_veiculo: "van"), 
      "Não criou a rota com van"
  end

end


