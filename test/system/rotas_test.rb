require "application_system_test_case"
require 'test_helper'

class RotasTest < ApplicationSystemTestCase
  setup do
    @rota = rotas(:usp)
  end

  test "visitando a home sem autenticação" do
    visit rotas_url
    assert_selector "h1", text: "Rotas"
    assert_selector "a", {text: "Nova", count: 0}
  end

  # test "visitando a home com autenticação" do
  #   #@request.env["HTTP_AUTHORIZATION"] = "Basic " + Base64::encode64("admin:admin")
  #   #page.driver.basic_auth('admin', 'admin')
  #   #request.headers['Authorization'] = ActionController::HttpAuthentication::Basic.encode_credentials('admin', 'admin')

  #   # name = 'admin'
  #   # password = 'admin'
  #   # if page.driver.respond_to?(:basic_auth)
  #   #   page.driver.basic_auth(name, password)
  #   # elsif page.driver.respond_to?(:basic_authorize)
  #   #   page.driver.basic_authorize(name, password)
  #   # elsif page.driver.respond_to?(:browser) && page.driver.browser.respond_to?(:basic_authorize)
  #   #   page.driver.browser.basic_authorize(name, password)
  #   # else
  #   #   raise "I don't know how to log in!"
  #   # end

  #   #page.driver.browser.authorize 'admin', 'admin'

  #   visit rotas_url
  #   assert_selector "h1", text: "Rotas"
  #   assert_selector "a", text: "Nova"
  # end

  # test "creating a Rota" do
  #   visit rotas_url
  #   click_on "New Rota"

  #   fill_in "Nome", with: @rota.nome
  #   fill_in "Partida", with: @rota.partida
  #   fill_in "Tipo veiculo", with: @rota.tipo_veiculo
  #   click_on "Create Rota"

  #   assert_text "Rota was successfully created"
  #   click_on "Back"
  # end

  # test "updating a Rota" do
  #   visit rotas_url
  #   click_on "Edit", match: :first

  #   fill_in "Nome", with: @rota.nome
  #   fill_in "Partida", with: @rota.partida
  #   fill_in "Tipo veiculo", with: @rota.tipo_veiculo
  #   click_on "Update Rota"

  #   assert_text "Rota was successfully updated"
  #   click_on "Back"
  # end

  # test "destroying a Rota" do
  #   visit rotas_url
  #   page.accept_confirm do
  #     click_on "Destroy", match: :first
  #   end

  #   assert_text "Rota was successfully destroyed"
  # end
end
