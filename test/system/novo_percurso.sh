
HOST=localhost

rota_id=1
psql -h localhost -p 5432 -U postgres -d rotas_dev -c "delete from percursos where rota_id = ${rota_id}"

printf "\nListando rotas disponíveis:\n"
curl -s http://$HOST:3000/rotas.json

percurso_id=`curl -X POST -i -s -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" http://$HOST:3000/percursos | grep Location | cut -d '/' -f 5`
percurso_id=${percurso_id%$'\r'}

printf "\n\nCriado percurso ${percurso_id}:\n"
curl -s -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" "http://$HOST:3000/percursos/${percurso_id}"

curl -X PATCH -s -o /dev/null -H "Content-Type: application/json" \
  -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" \
  "http://$HOST:3000/percursos/${percurso_id}" \
  -d '{"percurso": {"partida": [-23.529238, -46.812939]} }'

printf "\n\nPartida foi adicionada ao percurso ${percurso_id}:\n"
curl -s -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" "http://$HOST:3000/percursos/${percurso_id}"

curl -X PATCH -s -o /dev/null -H "Content-Type: application/json" \
  -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" \
  "http://$HOST:3000/percursos/${percurso_id}" \
  -d '{"percurso": {"waypoints": [[-23.556580, -46.750236, "parada"],[-23.557180, -46.740599, "parada"], [-23.553879, -46.733831], [-23.652897, -46.721843]]} }'

printf "\n\nWaypoints foram adicionados ao percurso ${percurso_id}:\n"
curl -s -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" "http://$HOST:3000/percursos/${percurso_id}"

curl -X PATCH -s -o /dev/null -H "Content-Type: application/json" \
  -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" \
  "http://$HOST:3000/percursos/${percurso_id}" \
  -d '{"percurso": {"destino": [-23.674728, -46.708772]} }'

printf "\n\nDestino foi adicionado ao percurso ${percurso_id}:\n"
curl -s -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" "http://$HOST:3000/percursos/${percurso_id}"

curl -X PATCH -s -o /dev/null -H "Content-Type: application/json" \
  -H "Authorization: Token token=$ROTAS_COMMUNITY_TOKEN" \
  "http://$HOST:3000/percursos/${percurso_id}" \
  -d '{"percurso": {"rota_id": '$rota_id'} }'

printf "\n\nPercurso ${percurso_id} foi atribuído à rota ${rota_id}\n"
