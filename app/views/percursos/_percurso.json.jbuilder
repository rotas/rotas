json.extract! percurso, :id, :partida, :destino, :waypoints, :created_at, :updated_at
json.url percurso_url(percurso, format: :json)
