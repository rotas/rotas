function showRotaOnMaps(partida, destino, waypoints) {

  var map = new google.maps.Map(
      document.getElementById('map'));

  var partida_latlng = new google.maps.LatLng(partida[0], partida[1])
  var destino_latlng = new google.maps.LatLng(destino[0], destino[1])

  var directionsService = new google.maps.DirectionsService();
  var directionsDisplay = new google.maps.DirectionsRenderer();
  directionsDisplay.setMap(map);

  waypoints_latlng = []
  waypoints.forEach(function(waypoint){
      waypoint_latlng = {'location': new google.maps.LatLng(waypoint[0], waypoint[1]),
                         'stopover': false}
      if (waypoint[2] !== void 0 && waypoint[2] == 'parada') {
        waypoint_latlng['stopover'] = true
      }
      waypoints_latlng.push(waypoint_latlng);
  });
  var request = {
    origin: partida_latlng,
    destination: destino_latlng,
    waypoints: waypoints_latlng,
    travelMode: 'DRIVING'
  };

  directionsService.route(request, function(result, status) {
    if (status == 'OK') {
      directionsDisplay.setDirections(result);
    }
  });

}
