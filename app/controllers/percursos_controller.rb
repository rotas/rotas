class PercursosController < ApplicationController

  before_action :set_percurso, only: [:show, :update]
  skip_before_action :verify_authenticity_token

  before_action :authenticate

  # POST /percursos
  def create
    @percurso = Percurso.new()

    respond_to do |format|
      if @percurso.save
        format.json { render :show, status: :created, location: @percurso }
      else
        format.json { render json: @percurso.errors, status: :unprocessable_entity }
    end
  end
  end

  # GET /percursos/1
  def show
  end

  # PATCH/PUT /rotas/1
  def update
    respond_to do |format|
      if update_rota
        if @percurso.update(percurso_params)
          format.json { render :show, status: :ok, location: @percurso }
        else
          format.json { render json: @percurso.errors, status: :unprocessable_entity }
        end  
      else
        rota_id = params[:percurso][:rota_id]
        format.json { render json: "Rota #{rota_id} inexistente", status: :unprocessable_entity }
      end
    end
  end

  protected
    def authenticate
      authenticate_or_request_with_http_token do |token, options|
        token == Rails.configuration.community_token
      end
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_percurso
      @percurso = Percurso.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def percurso_params
      # não funciona porque waypoints é array de arrays
      #params.require(:percurso).permit(:rota_id, :partida  => [], :destino => [], :waypoints => [])
      params.require(:percurso).permit!
      # https://medium.com/@apneadiving/why-i-do-not-use-strong-parameters-in-rails-e3bd07fcda1d
      params.require(:percurso).slice('partida', 'destino', 'waypoints', 'rota_id')
    end

    def update_rota
      rota_id = params[:percurso][:rota_id]
      if rota_id
        rota =  Rota.find_by_id(rota_id)
        # Obs: Rota.find_by_id(rota_id) retorna nil se rota não existir.
        # Já Rota.find(rota_id) joga uma exceção se rota não existir.
        if rota
          rota.update(percurso: @percurso)
        else
          return false
        end
      end
      return true
    end

end
