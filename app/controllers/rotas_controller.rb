class RotasController < ApplicationController

  before_action :set_rota, only: [:show, :edit, :update, :destroy]

  http_basic_authenticate_with name: "admin",
    password: Rails.configuration.admin_password, except: [:index, :show]

  # GET /admin
  # Autentica e joga para /rotas
  def admin
    redirect_to rotas_path
  end

  # GET /rotas
  # GET /rotas.json
  def index
    @rotas = Rota.all
    respond_to do |format|
      format.html {}
      format.json { render json: @rotas, :except => [:created_at, :updated_at] }
    end
  end

  # GET /rotas/1
  # GET /rotas/1.json
  def show
    @google_maps_key = Rails.configuration.google_maps_key
    respond_to do |format|
      format.html {}
      format.json { render json: @rota, :except => [:created_at, :updated_at] }
    end
  end

  # GET /rotas/new
  def new
    @rota = Rota.new
  end

  # GET /rotas/1/edit
  def edit
  end

  # POST /rotas
  # POST /rotas.json
  def create
    @rota = Rota.new(rota_params)

    respond_to do |format|
      if @rota.save
        format.html { redirect_to @rota, notice: 'Rota was successfully created.' }
        format.json { render :show, status: :created, location: @rota }
      else
        format.html { render :new }
        format.json { render json: @rota.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rotas/1
  # PATCH/PUT /rotas/1.json
  def update
    respond_to do |format|
      if @rota.update(rota_params)
        format.html { redirect_to @rota, notice: 'Rota was successfully updated.' }
        format.json { render :show, status: :ok, location: @rota }
      else
        format.html { render :edit }
        format.json { render json: @rota.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rotas/1
  # DELETE /rotas/1.json
  def destroy
    @rota.destroy
    respond_to do |format|
      format.html { redirect_to rotas_url, notice: 'Rota was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rota
      @rota = Rota.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rota_params
      params.require(:rota).permit(:nome, :partida, :tipo_veiculo)
    end

end
