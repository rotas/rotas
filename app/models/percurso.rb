class Percurso < ApplicationRecord

  serialize :partida, Array
  serialize :destino, Array
  serialize :waypoints, Array

end
