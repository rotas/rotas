class Rota < ApplicationRecord

  has_one :percurso

  def self.tipos_veiculos_permitidos
    %w(van ônibus)
  end

  validates :nome, presence: true

  validates :tipo_veiculo, inclusion: { in: self.tipos_veiculos_permitidos,
    message: "%{value} deve ser 'van' ou 'ônibus'" }

  validates :partida, format: { with: /\A[0-2][0-9]:[0-5][0-9]\z/,
    message: "deve estar no formato hh:mm" }



end
