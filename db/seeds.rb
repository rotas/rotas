# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Rota.destroy_all
Rota.create(nome: 'Quitaúna => Serpro', partida: '07:00', tipo_veiculo:'ônibus')
Rota.create(nome: 'Serpro => Quitaúna', partida: '17:10', tipo_veiculo:'ônibus')

cavalo = [-23.557298, -46.733968]
p1 = [-23.564739, -46.712535]
waypoints = [[-23.557586, -46.733380], # av luciano gualberto
              [-23.558003, -46.732557], # av luciano gualberto
              [-23.558510, -46.731559, 'parada'], # IME
              [-23.559075, -46.730406], # av luciano gualberto
              [-23.559449, -46.729649, 'parada'], # FAU
              [-23.559813, -46.728888], # av luciano gualberto
              [-23.560206, -46.728088], # av luciano gualberto
              [-23.560472, -46.727541], # av luciano gualberto
              [-23.561062, -46.726323], # av luciano gualberto
              [-23.561632, -46.725175, 'parada'], # FFLCH
              [-23.562203, -46.724011], # av luciano gualberto
              [-23.562951, -46.722456], # av luciano gualberto
              [-23.563841, -46.721158], # rotatória
              [-23.563182, -46.720369, 'parada'], # casa de cultura japonesa
              [-23.562715, -46.719479], # av lineu prestes
              [-23.562573, -46.718679], # rotatória
              [-23.562750, -46.718030], # av da universidade
              [-23.563153, -46.717231], # av da universidade
              [-23.563485, -46.716548], # av da universidade
              [-23.563485, -46.716548, 'parada'], # paço das artes
              [-23.564608, -46.714188], # av da universidade
              [-23.565254, -46.713449], # rotatória
              ]

percurso = Percurso.create(partida: cavalo, destino:p1, waypoints:waypoints)
Rota.create(nome: 'Cavalo => P1', tipo_veiculo:'ônibus', partida:'14:00', percurso:percurso)

serpro = [-23.674728, -46.708772]
luz = [-23.537354, -46.629285]
waypoints = [[-23.664644, -46.708081],
                        [-23.663306, -46.710167, 'parada'], # estação socorro
                        [-23.576532, -46.640911, 'parada'], # paraíso
                        ]
percurso  = Percurso.create(partida: serpro, destino:luz, waypoints:waypoints)
Rota.create(nome: 'Serpro => Luz', partida: '14:10', tipo_veiculo:'van', percurso:percurso)
