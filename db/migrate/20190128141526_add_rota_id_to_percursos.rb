class AddRotaIdToPercursos < ActiveRecord::Migration[5.2]
  def change
    add_column :percursos, :rota_id, :integer
  end
end
