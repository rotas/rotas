class CreatePercursos < ActiveRecord::Migration[5.2]
  def change
    create_table :percursos do |t|
      t.string :partida
      t.text :destino
      t.text :waypoints

      t.timestamps
    end
  end
end
