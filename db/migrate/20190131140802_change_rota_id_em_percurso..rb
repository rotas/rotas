class ChangeRotaIdEmPercurso < ActiveRecord::Migration[5.2]
  def change
    remove_column :percursos, :rota_id
    add_reference :percursos, :rota, index: true
  end
end
