class ChangeDestinoToStringInPercurso < ActiveRecord::Migration[5.2]
  def change
    change_column :percursos, :destino, :string
  end
end
