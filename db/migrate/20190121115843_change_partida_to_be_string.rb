class ChangePartidaToBeString < ActiveRecord::Migration[5.2]
  def change
     change_column :rotas, :partida, :string
  end
end
