class CreateRotas < ActiveRecord::Migration[5.2]
  def change
    create_table :rotas do |t|
      # TODO definir "enum" tipo_veiculo (van, onibus)
      t.string :nome
      t.time :partida
      t.string :tipo_veiculo

      t.timestamps
    end
  end
end
