# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_31_140802) do

  create_table "percursos", force: :cascade do |t|
    t.string "partida"
    t.string "destino"
    t.text "waypoints"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "rota_id"
    t.index ["rota_id"], name: "index_percursos_on_rota_id"
  end

  create_table "rotas", force: :cascade do |t|
    t.string "nome"
    t.string "partida"
    t.string "tipo_veiculo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
